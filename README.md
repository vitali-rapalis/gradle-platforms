# [Gradle Platforms](https://docs.gradle.org/current/userguide/java_platform_plugin.html)

---

> **The Java Platform Plugin**. <br><br>
> The Java Platform plugin brings the ability to declare platforms for the Java ecosystem. A platform can be used for different purposes:
> - a description of modules which are published together (and for example, share the same version)
> - a set of recommended versions for heterogeneous libraries. A typical example includes the Spring Boot BOM
> - sharing a set of dependency versions between subprojects


## Spring Boot Product Platform
How to use spring boot custom product platform in another java projects. Can be used with gradle catalogs. 
```
settings.gradle

dependencyResolutionManagement {
    repositories {
        mavenCentral()
        maven {
            url 'https://gitlab.com/api/v4/projects/43975196/packages/maven'
        }
        maven {
            url 'https://gitlab.com/api/v4/projects/43975633/packages/maven'
        }
    }

    versionCatalogs {
        springBootCatalog {
            from("com.konigsberger.www.gradle-catalogs:spring-catalog:0.0.1")
        }
    }
}
```

```
build.gradle

plugins {
    id "java-library"
}

dependencies {
    // get recommended versions from the product platform project
    api platform('com.konigsberger.www.gradle-platforms:spring-boot-product-platform:0.0.1')

    // no version required
    // gradle catalog
     api springBootCatalog.web
}
```
